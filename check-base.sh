#!/bin/sh
#

checkBase()
{
    local baseImage=$1
    local imageToCheck=$2

    docker pull $baseImage
    docker pull $imageToCheck

    local baseLayers=$(docker image inspect --format "{{.RootFS.Layers}}" $baseImage | sed -n "s/\[\(.*\)\]/\1/p")
    local toCheckLayers=$(docker image inspect --format "{{.RootFS.Layers}}" $imageToCheck | sed -n "s/\[\(.*\)\]/\1/p")

    case $toCheckLayers in
    *$baseLayers*   ) return 0 ;;
    *               ) return 1 ;;
    esac
}

checkBase $MY_BASE_IMAGE $MY_IMAGE
ret=$?

echo $ret > shouldUpdate
