#!/bin/sh
#

shouldUpdate=$(cat shouldUpdate)

if [[ $shouldUpdate == 1 ]]
then
    echo "should be updated. Triggering..."
    sh -c "curl --request POST --form token=$MY_REPO_TOKEN --form ref=$MY_REPO_REF $MY_PAYLOAD $MY_REPO_URL"
else
    echo "no update needed"
fi
